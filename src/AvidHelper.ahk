; AVIDHELPER 1.0.3
; for Avid 5.x
; (c) 2013 Christian Moe

#SingleInstance

;Check if Avid 5 is open, and activate the window
IfWinExist, ahk_class AvidEditorMainWndClass
{
	WinActivate, ahk_class AvidEditorMainWndClass 
}
;Check if Avid 6 is open
else IfWinExist, ahk_class QWidget
{
	MsgBox AvidHelper does not support Avid Media Composer 6. It has not been tested, and it is not advised to use AvidHelper with this version.
}
else
{
	MsgBox No supported Avid Editor found. AvidHelper needs Avid 5 to work.
}

;Prioritise this script over other processes
Process, Priority, , High


;***********
;SETTINGS
;***********

;Initiate setting types
settingTypes := Object()
settingTypes := {frameRate: "int",	shortKeyDur 	: "int", instantKeyDur 	: "int",	longKeyDur 		: "int",	quickMode 		: "bool",		groupAudio 		: "bool",	insertGroup 	: "bool",	deleteSubclips 	: "bool",	autoClipDuration: "bool",	autoCountTracks : "bool",	displayMouse 	: "bool",	timeCodeColor 	: "color",	indicatorColor	: "color",	fillerColor		: "color",	dividerColor	: "color",	backgroundColor	: "color"}

;Open the saved settings file, if it exists. Else open the standard config file
file := FileOpen("SavedSettings.cfg", "r")

if(!IsObject(file))
{
	file := FileOpen("AvidHelperSettings.cfg", "r")

	if(!IsObject(file))
	{
		MsgBox Can't find AvidHelperSettings.cfg.
		Exit
	}
}

;This is used to check which settings are already loaded
remSettings := settingTypes.clone()

;Read the config file line by line, and check for settings
Loop 
{
	line := file.ReadLine()

	;Skip comment lines
	if(!InStr(line, ";"))
	{
		;Check if a setting is on this line
		For settingName, type in settingTypes
		{
			;Don't check settings, which are already set. Also there is an empty settingName...dunno where it comes from
			if(settingName != "" && remSettings[settingName])
			{
				;Get the settings value from the line (if any), and check that it corresponds with the type
				settingValue := getSetting(line, settingName, type)
				
				;getSetting will return -1 if no setting is found on the line
				if(settingValue != -1)
				{
					;A variable with the setting name is set for easy usage throughout the script
					%settingName% := settingValue
					
					remSettings.Remove(settingName)
				}
			}
		}
	}
		
	if(file.AtEOF)
	{
		break
	}
}

; Check that the settings are not empty
For settingName, type in settingTypes
{
	if(!settingName || settingName = "" )
	{
		MsgBox Can't find the setting %settingName% in AutoGroupsSettings.cfg. AvidHelper aborted.
		Exit
	}
}

;Check if the files for the autoClipDuration exists, if needed
if(autoClipDuration && !quickMode)
{
	if(!(InStr( FileExist("searchFiles"), "D")))
	{
		MsgBox The directory "searchFiles" could not be found. AvidHelper aborted.
		Exit
	}

	if(!FileExist("searchFiles/centerDuration.jpg"))
	{
		MsgBox The file "searchFiles/centerDuration.jpg" could not be found. AvidHelper aborted
		Exit
	}

	Loop, 10
	{
		fileName := "searchFiles/" . (A_Index - 1) . ".jpg"
		if(!FileExist(fileName))
		{
			MsgBox The file "%fileName%" could not be found. AvidHelper aborted
			Exit
		}
	}
}

;Key-list used in the AvidHelper profile
plusSign			:= "{NumpadAdd}"
minusSign			:= "{NumpadSub}"
numEnter			:= "{NumpadEnter}"
escape				:= "{Esc}"
enter				:= "{Enter}"
tab					:= "{Tab}"
space				:= "{Space}"
delete				:= "{Delete}"

downArrow			:= "{Down}"
selectDown			:= "+{Down}"

focusTimeline 		:= "{F9}"
focusComposer		:= "^4"

clearInOut			:= "g"
markClip			:= "t"
gotoInPoint			:= "q"
gotoOutPoint		:= "w"
gotoEnd				:= "{End}"
gotoStart			:= "{Home}"
setOutPoint			:= "o"

selectAllTracks		:= "^a"
deselectAllTracks 	:= "+�"
selectA1			:= "+0"
selectV1			:= "+1"

matchFrame			:= "{F5}"
findBin				:= "{F6}"
subClip				:= "{F7}"

groupClips			:= "+g"
defaultSetup		:= "+d"
duplicate			:= "^d"
showInfo			:= "^i"
closeWindow			:= "^w"
alignColumns		:= "^t"

focus				:= "h"
overwriteEdit		:= "b"
fastForward			:= "s"
lessDetail			:= "e"
moreDetail			:= "r"

createVTrack		:= "^y"
save				:= "^s"
reduceTrack			:= "^k"
enlargeTrack		:= "^l"


Gui, Configurate:New, , Welcome

Gui, Font, W700
Gui, Add, Text,, `nWelcome to AvidHelper v.1.0.3 for Avid 5.x

Gui, Font, W400

Gui, Add, Text,, Remember to import user profile AvidHelper1.0 to Avid and select it, before using AvidHelper
Gui, Add, Text,, NOTE: If you're running Avid with admin privileges, you need to run AvidHelper as Administrator as well

Gui, Add, Text,, INSTRUCTIONS FOR AUTOGROUP

Gui, Add, Text,, 1) Prepare the sequence, so that the longest clip for each group is in V1
Gui, Add, Text,, 2) Make sure all clips are aligned to the beginning of the longest clip

Gui, Add, Text,, -------------

Gui, Add, Text,, Configure:		Ctrl+Shift+C`nAutoGroup:		Ctrl+Shift+G`nAutoSubClip:		Ctrl+Shift+S`nQuickSub:		�`nStop and reload script:	Ctrl+Alt+Shift+A

Gui, Show, AutoSize Center




; Configure thisinstance
^+c::

	Gui, Configurate:New, , Configuration

	Gui, Font, W700
	Gui, Add, Text,, `nAutoGroup Settings

	Gui, Font, W400
	Gui, Add, Text,, Quick mode omits the out-point, making groups longer than the clips in the sequence, but processes faster
	checked := quickMode? "Checked" : ""
	Gui, Add, Checkbox, %checked% vquickMode, quickMode

	Gui, Add, Text,, -------------

	Gui, Add, Text,, Count amount of video tracks to group automatically
	checked := autoCountTracks? "Checked" : ""
	Gui, Add, Checkbox, %checked% vautoCountTracks, autoCountTracks

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, Group audio in track A1 with video
	checked := groupAudio? "Checked" : ""
	Gui, Add, Checkbox, %checked% vgroupAudio, groupAudio

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, Overwrite clips in sequence with group`n(NOTE: If groupAudio is true, and there are more than 9 video tracks, all upper tracks will be overwritten)
	checked := insertGroup? "Checked" : ""
	Gui, Add, Checkbox, %checked% vinsertGroup, insertGroup

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, Deletes the subclips used for grouping, after the group is created
	checked := deleteSubclips? "Checked" : ""
	Gui, Add, Checkbox, %checked% vdeleteSubclips, deleteSubclips

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, When not in quick mode, automatically get the clip duration
	checked := autoClipDuration? "Checked" : ""
	Gui, Add, Checkbox, %checked% vautoClipDuration, autoClipDuration

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, Display mouse movement, while analyzing screen
	checked := displayMouse? "Checked" : ""
	Gui, Add, Checkbox, %checked% vdisplayMouse, displayMouse

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, Set framerate of project. Only used for calculating clip duration
	Gui, Add, Edit, r1 Limit2 Number vframeRate, %frameRate%

	Gui, Font, W700
	Gui, Add, Text,, `nAdvanced AutoGroup Settings

	Gui, Font, W400
	Gui, Add, Text,, Set key press durations in milliseconds for Avid actions that have no processing time
	Gui, Add, Edit, r1 Limit2 Number vinstantKeyDur, %instantKeyDur%

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, Set key press durations in milliseconds for Avid actions that have a little processing time
	Gui, Add, Edit, r1 Limit3 Number vshortKeyDur, %shortKeyDur%

	Gui, Add, Text,, -------------

	Gui, Font, W400
	Gui, Add, Text,, Set key press durations in milliseconds for Avid actions that have a longer processing time
	Gui, Add, Edit, r1 Limit4 Number vlongKeyDur, %longKeyDur%

	Gui, Add, Text,, -------------



	Gui, Add, Button, Default gSave x10, Save Settings
	Gui, Add, Button, gRevert xp+100, Default settings
	Gui, Add, Button, gCancel xp+100, Cancel

	Gui, Show, AutoSize Center
	return  ; End of auto-execute section. The script is idle until the user does something.


	Cancel:
		Gui, Cancel
		Exit

	Revert:
		Gui, Cancel
		FileDelete, SavedSettings.cfg
		Reload
		Exit

	Save:
		Gui, Submit
		file := FileOpen("SavedSettings.cfg", "w")

		if(!IsObject(file))
		{
			MsgBox Can't open SavedSettings.cfg for writing.
			Exit
		}

		For settingName, value in settingTypes
		{
			text := ""
			if(settingName != "")
			{
				text := settingName . " := " . %settingName%
				file.WriteLine(text)
			}
		}

		file.Close()

		MsgBox, Configuration saved.
		Exit	



; Auto Subclipper
^+s::

	allClips := false

	if(autoCountSubclips)
	{
		MsgBox , 4096, Perform AutoSubclip, AutoCountSubclips not available at the moment. Select the top clip and press OK to start AutoSubclip
		
		InputBox, numFiles, Number of clips, Enter amount of clips to process

		if(Errorlevel)
		{
			MsgBox AutoSubclip aborted.
			Exit
		}
		
		autoCountSubclips := false
	}
	else
	{
		MsgBox , 4096, Perform AutoSubclip, Select the top clip and press OK to start AutoSubclip
		
		InputBox, numFiles, Number of clips, Enter amount of clips to process

		if(Errorlevel)
		{
			MsgBox AutoSubclip aborted.
			Exit
		}
	}

	if(allClips && autoCountSubclips)
	{
		MsgBox, 4096, Perform AutoSubclip, Please select the first clip in the bin and press OK to continue
				
		IfMsgBox Cancel
		{
			MsgBox AutoSubclip aborted.
			Exit
		}
	}

	if(numFiles = 1)
	{
		if(!allClips && autoCountSubclips)
		{
			SetKeyDelay, longKeyDur
			Send %duplicate%
			SetKeyDelay, shortKeyDur
		}
		
		Send %numEnter%
		Send %enter%
		
		Send %clearInOut%
		Send %markClip%
		Send %subClip%
		
		Send %numEnter%
		Send %findBin%
		
		Send %downArrow%
		
		if(!allClips && autoCountSubclips)
		{
			Send %delete%
			Send %space%
			Send %tab%
			Send %tab%
			Send %enter%
		}
	}
	else
	{
		i := 2
		j := 1

		if(!allClips && autoCountSubclips)
		{
			SetKeyDelay, longKeyDur
			Send %duplicate%
			
			SetKeyDelay, shortKeyDur
			Send {Down}
			Send {Up %numFiles%}
		}
			
		Loop, %numFiles%
		{
			if(j < 3)
			{
				SetKeyDelay, longKeyDur
			}
			else
			{
				SetKeyDelay, shortKeyDur
			}
			
			Send %enter%
			
			Send %clearInOut%
			Send %markClip%
			Send %subClip%
			
			Send %numEnter%
			Send %findBin%
			
			if(j < numFiles)
			{
				if(j < 3)
				{
					SetKeyDelay, shortKeyDur
				}
				else
				{
					SetKeyDelay, instantKeyDur
				}
				Send {Down %i%}
			}
			
			i += 2
			j++
		}

		if(!allClips && autoCountSubclips)
		{
			SetKeyDelay, shortKeyDur
			Send {Down %numFiles%}

			;Remove 1 from numfiles to select the duplicates
			numFiles--
			Send +{Down %numFiles%}
			Send %delete%
			Send %space%
			Send %tab%
			Send %tab%
			Send %enter%
		}
	}
		
	MsgBox, AutoSubClip finished
	Exit

; AutoGroup
^+g::

	;Maximum amount of clips that can be grouped
	maxClips			:= 9 

	MsgBox , 1, Perform AutoGroup, Press OK to start AutoGroup

	IfMsgBox Cancel
	{
		MsgBox AutoGroup aborted.
		Exit
	}

	if(autoCountTracks = true)
	{
		trackHeight := 0 ;Height of a full track
		
		MouseGetPos curX, curY
		
		SetKeyDelay, shortKeyDur

		;Focus and show the entire timeline
		Send %focusTimeline%
		Send %deselectAllTracks%

		;Make sure all tracks have same height, and no additional colors are added
		Send %defaultSetup%
		
		Send %focus%
		Send %clearInOut%
		
		;Make sure the timeline indicator is at the start of the clip
		Send %selectV1%
		Send %markClip%
		Send %gotoInPoint%
		Send %clearInOut%
		Send %deselectAllTracks%

		;Create two tracks for calibration
		Send %createVTrack%
		Send %createVTrack%
		
		;Searcharea around the mouse position
		searchArea := 400
		
		;Find the timecode box
		Loop
		{	
			PixelSearch timecodeX, timecodeY, (curX-searchArea), (curY-searchArea), (curX + searchArea), (curY + searchArea), %timecodeColor%, 0, Fast
			
			;If the local search does not work, search the entire screen
			if(ErrorLevel)
			{
				PixelSearch timecodeX, timecodeY, 0, 0, A_ScreenWidth, A_ScreenHeight, timecodeColor, 0, Fast
				
				if(ErrorLevel)
				{
					MsgBox, Could not find timecode window. AutoGroup aborted.
					Exit
				}
				else
				{
					break
				}
			}
			else
			{
				break
			}
		}
		
		if(displayMouse)
		{
			;Move the mouse, so the search can be followed visually
			MouseMove timecodeX, timecodeY
		}
		
		curX := timecodeX
		curY := timecodeY
		
		;Find the current timeline position
		Loop
		{
			PixelSearch indicatorX, indicatorY, curX,curY, A_ScreenWidth, (curY + searchArea), %indicatorColor%, 0, Fast
			
			if ErrorLevel
			{
				MsgBox, Could not find timeline indicator. AutoGroup aborted.
				Exit
			}
			else
			{
				break
			}
		}
		
		curX 	:= indicatorX + 5
		curY 	:= indicatorY

		;Make sure we're free of the timeline indicator
		Loop
		{
			PixelGetColor, curColor, curX, curY
		
			if(curColor = indicatorColor)
			{
				curX++
			}
			else
			{
				break
			}
		}
		
		if(displayMouse)
		{
			MouseMove (indicatorX + 5), indicatorY
		}
		
		startX := curX
		
		;Find the top empty track
		coord := findColor(fillerColor, 10)
		
		;Find the divider between tracks, to determine track height
		coord := findColor(dividerColor, 1)
		
		;Get track top position
		startY := coord[Y]
		
		;Find the next divider
		coord := findColor(dividerColor, 1)
			
		;Get track bottom position
		endY := coord[Y]

		;Set default values
		numTracks 	:= 0
		videoColor 	:= 0x000000

		trackHeight := endY - startY
		
		;Clean up the calibration tracks
		Send %delete%
		Send %enter%
		
		abortBox("Initialising...", 1)
	}

	;A block is the clips in the sequence to be grouped
	groupedBlocks 	:= 0
	blocksToGroup 	:= -1

	;Process a block
	Loop
	{
		start:
		
		SetKeyDelay, longKeyDur
		
		;Stop prompting for amount of blocks to group, if script should continue excecuting until end of sequence
		if(blocksToGroup = 0)
		{
			groupedBlocks := -1
		}
		
		;Ask for new inputs, when the requested amount of processed blocks are reached
		if(blocksToGroup <= groupedBlocks || autoCountTracks)
		{
			SetKeyDelay, shortKeyDur
			
			if(blocksToGroup <= groupedBlocks)
			{
				;Input the amount of blocks to process
				InputBox, blocksToGroup, Enter amount of blocks to process (0 for all)

				if(ErrorLevel)
				{
					MsgBox, AutoGroup aborted
					Exit
				}

				;Reset number of grouped blocks
				groupedBlocks := 0
			}
			
			if(autoCountTracks)
			{
				;For all but the first block, find the next timeline indicator
				if(groupedBlocks <> 0)
				{
					searchX := A_ScreenWidth
					searchY := A_ScreenHeight
									
					PixelSearch moveX, moveY, timecodeX,timecodeY, searchX, searchY, %indicatorColor%, 0, Fast
					
					if(ErrorLevel)
					{
						abortBox("Trying to locate next timeline indicator", 2)

						PixelSearch moveX, moveY, timecodeX,timecodeY, searchX, searchY, %indicatorColor%, 0, Fast
						
						if(ErrorLevel)
						{
							MsgBox, Could not find next timeline indicator. AutoGroup aborted.
							Exit
						}
					}
									
					if(displayMouse)
					{
						MouseMove moveX, moveY
					}
									
					startX := moveX + 5
									
					;Make sure we're free of the timeline indicator
					Loop
					{
						PixelGetColor, curColor, startX, curY
					
						if(curColor = indicatorColor)
						{
							startX++
						}
						else
						{
							break
						}
					}
					
					PixelGetColor, curColor, startX, curY
					
					;If the background is reached, end loop
					if(curColor = backgroundColor)
					{
						break
					}
					
					numTracks := 0
				}
			
				curX := startX
				curY := startY
				
				;Make sure to check middle of track
				curY += Floor(trackHeight/2)
				
				if(displayMouse)
				{
					MouseMove curX, curY
				}
				
				;Count number of video tracks
				Loop
				{
					PixelGetColor, curColor, curX, curY
					
					if(curColor != fillerColor)
					{
						if(videoColor = 0x000000)
						{
							videoColor := curColor
						}
						
						if(curColor != videoColor)
						{
							break
						}
						
						numTracks++
					}
					else
					{
						if(numTracks > 0)
						{
							Send %deselectAllTracks%
							Send %selectV1%
							Send %fastForward%
							groupedBlocks++
							Goto, start
						}
					}
					
					curY += trackHeight
					
					if(displayMouse)
					{
						MouseMove curX,curY
					}
				}
				
				if(numTracks > maxClips)
				{
					numTracks := maxClips
				}
				
				if(numTracks <= 1)
				{	
					Send %deselectAllTracks%
					Send %selectV1%
					Send %fastForward%
					groupedBlocks++
					Goto, start
				}
				
			}
			else
			{
				;V1.0 Track amount
				Loop
				{
					;Input the amount of tracks to be grouped
					InputBox, numTracks, Enter amount of tracks to be grouped (max. 9)

					if(ErrorLevel)
					{
						MsgBox, AutoGroup aborted
						Exit
					}
					
					if(numTracks > maxClips)
					{
						MsgBox, You have entered more than the allowed amount of tracks. Please re-enter.
					}
					else
					{
						break
					}
					
					if(numTracks <= 1)
					{	
						Send %deselectAllTracks%
						Send %selectV1%
						Send %fastForward%
						groupedBlocks++
						Goto, start
					}
				}
		
			}
		
			SetKeyDelay, longKeyDur
			
			;Increment the amount of tracks by 1, to include the audio track
			if (groupAudio)
			{
				numTracks++
			}
		} 

		curTrack 		:= 1
		clipDuration 	:= 0
		audioIsGrouped 	:= false

		Send %focusTimeline%

		;Make subclips of each video track
		Loop, %numTracks%
		{
			;If the cursor is in rename mode, this escapes it
			Send %escape%

			Send %focusTimeline%

			Send %deselectAllTracks%
			Send +%curTrack%

			;If no timecode is set, prepare the sync point
			if (clipDuration = 0)
			{
				;Make sure the Avid Application is in focus
				WinActivate, ahk_class AvidEditorMainWndClass 
				Send %focusTimeline%
				Send %markClip%

				Send %gotoInPoint%
				
				if (!quickMode)
				{
					if(autoClipDuration)
					{
						Send %focusComposer%
						
						;Find the Center Duration
						Loop
						{
							fileName := A_WorkingDir . "/searchFiles/centerDuration.jpg"
							ImageSearch, durationX, durationY, 0,0, A_ScreenWidth, A_ScreenHeight, %fileName%

							if ErrorLevel
							{
								MsgBox, Could not find center duration. AutoGroup aborted.
								Exit
							}
							else
							{
								break
							}
						}


						durationX -= 30
						durationY -= 10

						X := Object()

						i := 0

						;Search for each number
						while(i < 10)
						{
							sX 			:= durationX
							sY 			:= durationY
							ErrorLevel 	:= 0
							
							X[i] := -1
							
							;Search for the number multiple times
							while (ErrorLevel != 1)
							{
								fileName := A_WorkingDir . "/searchFiles/" . i . ".jpg"
								
								ImageSearch, tX, tY, sX, sY, (sX + 80), (sY + 30), *20 %fileName%
								
								if(displayMouse)
								{
									MouseMove tX, tY
								}
								
								if(ErrorLevel = 2)
								{
									MsgBox AutoGroup aborted. Could not search for timecode
									Exit
								}
								
								;Store the position(s) as a string in the corresponding array
								if(ErrorLevel = 0)
								{
									if(X[i] = -1)
									{
										X.Remove(i)
										X[i] := tX . "." . i
									}
									else
									{
										X[i] .= "_" . tX . "." . i
									}
								}
								
								;Increment the search area, to search beyond the first found number
								sX := tX + 5
								
							}
							
							i++
						}

						readout := ""

						for index, element in X 
						{
							if(element != -1)
							{
								readout .= element . "_"
							}
						}

						Sort readout, N D_

						StringSplit, times, readout, _

						i := 1

						clipDuration := ""

						Loop, %times0%
						{
							m := "times" . i
							n := %m%
							
							if(n != "")
							{
								p := SubStr(n, -0, 1)
								clipDuration .= p
							}
							
							i++
						}
						
						if(RegExMatch(clipDuration, "00$"))
						{
							clipDuration += -100 + frameRate - 1
							
						}
						else
						{
							clipDuration--
						}
						
						if(displayMouse)
						{
							MouseMove tX, Y
						}
					}
					else
					{
						InputBox, clipDuration, Enter clip duration (read from 'Center Duration')

						if ErrorLevel
						{
							MsgBox, AutoGroup aborted
							Exit
						}
					}
				}
				else
				{
					;Timecode is not needed, when in quick-mode
					clipDuration = 1
				}
			}

			;Add audio track to group
			if (groupAudio && !audioIsGrouped)
			{
				Send %deselectAllTracks%
				Send %selectA1%
				audioIsGrouped := true
				curTrack--
			}	

			Send %matchFrame%
			
			;Set a second sync-point, if neat groups are wished
			if (!quickMode)
			{
				Send %plusSign%
				
				SetKeyDelay, shortKeyDur
				
				Send %clipDuration%
				
				SetKeyDelay, longKeyDur
				
				Send %enter%
				
				Send %setOutPoint%
			}
					
			Send %subClip%
					
			curTrack++
		}

		;Escapes, if renaming subclip sets in
		Send %numEnter%
		Send %findBin%

		;When selecting subclips, the first clip is automatically selected
		clipCount := numTracks -1

		SetKeyDelay, shortKeyDur
		
		;Select all subclips
		Loop, %clipCount%
		{
			Send %selectDown%
		}

		Send %groupClips%
		
		;Aborts if group cannot be created NEEDS TO BE ADJUSTED
		;IfWinExist, ahk_class_ASI_THREED_
		;{
		;	MsgBox, AutoGroup aborted. There was a problem creating the group. Most likely a group clip in the sequence.
		;	return
		;}
		
		;Navigates the group menu to In as sync-point
		Send %tab%
		Send %tab%
		Send %space%

		;Confirm group
		Send %enter%
		
		;Escapes, if renaming group sets in, or loads group in source monitor
		Send %numEnter%
		
		;Load group in source monitor, if not already there (else does nothing)
		Send %enter%
		
		;Delete subclips after grouping
		if(deleteSubclips)
		{
			Send %findBin%
			
			;Highlight the first subclip
			Send %downArrow%
			
			;Select all subclips (including the already highlighted one)
			Loop, %clipCount%
			{
				Send %selectDown%
			}
			
			Send %delete%
			Send %tab%
			Send %enter%
		}

		SetKeyDelay, longKeyDur

		Send %focusTimeLine%

		Send %deselectAllTracks%

		curTrack := 1

		SetKeyDelay, shortKeyDur

		;Overwrite tracks with group
		if (insertGroup)
		{
			;If the audio is grouped as well, we're forced to select all tracks for the insert edit
			if (groupAudio)
			{
				Send %selectV1%
				Send %selectAllTracks%
			}
			else
			{
				Loop, %numTracks%
				{
					Send +%curTrack%
					curTrack++
				}
			}

			Send %overwriteEdit%
		}
		else
		{
			Send %selectV1%
			Send %fastForward%
		}

		Send %save%

		;Wait for the save window to finish
		WinWait, ahk_class_ASI_THREED_
		WinWaitClose  

		groupedBlocks++
	}

	MsgBox, AutoGroup finished
	Exit

;Reload Script
^!+a::
	MsgBox AvidHelper will now reload.
	Reload
	Exit

;QuickSub
�::
	SetKeyDelay, 200
	Send i
	Send ^0
	Send t
	Send b
	Send a

;******************
;FUNCTIONS
;******************

;Shows an info message with the ability to abort
abortBox(message, duration)
{
	#SingleInstance
	SetTimer, SetOKasAbort, 50 
	MsgBox, 0, Please wait, %message%, %duration%
	IfMsgBox, OK 
	{
		MsgBox, AvidHelper aborted
		Exit
	} 
}

SetOKasAbort: 
	IfWinNotExist, Please wait
	{
		return  ; Keep waiting.
	}
	
	SetTimer, SetOKasAbort, off 
	WinActivate 
	ControlSetText, Button1, &Abort 
	return

;Searches for a color below the current position in given increments
findColor(color, increment)
{
	Global curX, curY, displayMouse
	
	; Create array for coordinates
	foundCoord := {X: 0, Y: 0}

	Loop
	{
		curY += increment

		PixelGetColor, curColor, curX, curY
		foundCoord[X] := curX
		foundCoord[Y] := curY
		
		if(displayMouse)
		{
			MouseMove curX,curY
		}
		
		if(curColor = color)
		{
			return foundCoord
		}
		
	}
}

;Get a setting from the setting file
getSetting(line, settingName, type = "bool")
{
	searchRegex := "i)^" . settingName
	search 		:= RegExMatch(line, searchRegex)
	
	RegExMatch(line, ":=(.*)", setting)
	
	;Retrieve trimmed setting from the array-result
	setting := trim(setting1)
	
	;Remove line feeds any line feed
	StringReplace, setting, setting, `r`n, , All
	setting := RegExReplace(setting, "\R", "")
	
	if(search >= 1)
	{
		if(type = "int")
		{
			if(setting is integer)
			{
				return setting
			}
			else
			{
				MsgBox The setting "%settingName%" is not an integer. AvidHelper aborted. (Current value is: "%setting%")
				Exit
			}
		}
		else if (type = "color")
		{
			 if(RegExMatch(setting, "^(0x[A-Fa-f0-9]{6})$"))
			{
				return setting
			}
			else
			{
				MsgBox The setting "%settingName%" is not of the proper color format. Must be of the format: "000000". AvidHelper aborted. (Current value is: "%setting%")
				Exit
			}
			
		}
		else
		{
			if(setting = "true" || setting = "1")
			{
				return true
			}
			else if(setting = "false" || setting = "0")
			{
				return false
			}
			else
			{
				MsgBox The setting "%settingName%" must either be set to true or false. AvidHelper aborted. (Current value is: "%setting%")
				Exit
			}
			
		}
	}
	
	return -1
}

